package si.uni_lj.fri.pbd.okhttpexamplekotlin

import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import si.uni_lj.fri.pbd.okhttpexamplekotlin.databinding.ActivityMainBinding
import java.io.IOException


class MainActivity : AppCompatActivity() {

    companion object {
        const val URL = "https://cat-fact.herokuapp.com/facts"
        const val TAG = "MainActivity"
    }

    private lateinit var binding: ActivityMainBinding

    var mArrlist = ArrayList<String>()

    var mAdapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        mAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mArrlist)
        binding.listview.adapter = mAdapter

        binding.button.setOnClickListener {
            getAsyncCall(URL)
        }
    }

    private fun getAsyncCall(myUrl: String) {


    }
}